# -*- coding: utf-8 -*-
import os
import re
import base64
import random
import string
import logging
import openfoodfacts
from random import randint
from itertools import count
from string import ascii_lowercase
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization


def generate_key_pair(key_length):
    private_key = rsa.generate_private_key(backend=default_backend(),
                                           public_exponent=65537,
                                           key_size=key_length)

    private_key_pem = private_key.private_bytes(encoding=serialization.Encoding.PEM,
                                                format=serialization.PrivateFormat.PKCS8,
                                                encryption_algorithm=serialization.NoEncryption())

    public_key = private_key.public_key()

    public_key_pem = public_key.public_bytes(serialization.Encoding.PEM,
                                             serialization.PublicFormat.SubjectPublicKeyInfo)

    return (public_key_pem, private_key_pem)


def store_keys(keys_tuple):
    public_key = keys_tuple[0]
    private_key = keys_tuple[1]
    with open('public_key.pem', 'wb') as f:
        f.write(public_key)

    with open('private_key.pem', 'wb') as f:
        f.write(private_key)


def read_keys():
    with open("private_key.pem", "rb") as key_file:
        private_key = serialization.load_pem_private_key(
            key_file.read(),
            password=None,
            backend=default_backend()
        )

    with open("public_key.pem", "rb") as key_file:
        public_key = serialization.load_pem_public_key(
            key_file.read(),
            backend=default_backend()
        )

    return (public_key, private_key)


def rsa_encrypt(data, public_key, b64_encode=False):
    encoded_data = to_binary(data)
    encrypted_data = public_key.encrypt(encoded_data,
                                        padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()),
                                                     algorithm=hashes.SHA256(),
                                                     label=None)
                                        )
    if b64_encode:
        encrypted_data = base64.b64encode(encrypted_data).decode("utf-8")
    return encrypted_data


def rsa_decrypt(encrypted_data, private_key, b64_decode=False, password=None):
    if b64_decode:
        encrypted_data = base64.b64decode(encrypted_data)
    encrypted_data = to_binary(encrypted_data)
    original_data = private_key.decrypt(encrypted_data,
                                        padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()),
                                                     algorithm=hashes.SHA256(),
                                                     label=None)
                                        )
    return original_data


def to_text(value, encoding="utf-8"):
    """Convert value to unicode, default encoding is utf-8

    :param value: Value to be converted
    :param encoding: Desired encoding
    """
    if not value:
        return ""
    if isinstance(value, str):
        return value
    if isinstance(value, bytes):
        return value.decode(encoding)
    return str(value)


def to_binary(value, encoding="utf-8"):
    """Convert value to binary string, default encoding is utf-8

    :param value: Value to be converted
    :param encoding: Desired encoding
    """
    if not value:
        return b""
    if isinstance(value, bytes):
        return value
    if isinstance(value, str):
        print(value.encode(encoding))
        return value.encode(encoding)
    return to_text(value).encode(encoding)


def text_obfuscate(data, cipher_filename, b64_encode=True):
    array64 = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/+=")
    data_obfuscate = []
    try:
        with open(cipher_filename) as file:
            cipher_array = [line.rstrip() for line in file.readlines()]
    except:
        logging.error('Problem reading cipher %s ! Verify the location of the cipher file', cipher_filename)
    for char in data:
        data_obfuscate.append(cipher_array[array64.index(char)])
    return data_obfuscate


def text_desobfuscate(data_obfuscate, cipher_filename, b64_encode=True):
    array64 = list("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/+=")
    data = ""
    try:
        with open(cipher_filename) as file:
            cipher_array = [line.rstrip() for line in file.readlines()]
    except:
        logging.error('Problem reading cipher %s ! Verify the location of the cipher file', cipher_filename)
    for word in data_obfuscate:
        data += array64[cipher_array.index(word)]
    return data


def random_with_n_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)


def generate_product_name(length):
    VOWELS = "aeiou"
    CONSONANTS = "".join(set(string.ascii_lowercase) - set(VOWELS))
    word = ""
    for i in range(length):
        if i % 3 == 0:
            word += random.choice(CONSONANTS)
        else:
            word += random.choice(VOWELS)
    return word


def generate_emb_codes():
    letter_mapping = dict(zip(count(0), ascii_lowercase))
    u0 = random.randint(0, 9)
    u1 = random.randint(0, 9)
    u2 = random.randint(0, 9)
    nb1 = (u0 + u1 - u2) % 10
    nb2 = ((2 * u0) + (3 * u1**2) - (35 * u2**5)) % 10
    nb3 = (u0**2 + u1**3 + u2**4) % 26
    return "EMB {}{}{}{}{}{}".format(nb1, u0, nb2, u1, u2, letter_mapping[nb3].upper())


def check_emb_codes(text):
    if len(text) != 10:
        return False
    elif not text.startswith("EMB "):
        return False
    else:
        result = re.match(r'EMB ([0-9]{5}[A-Z])', text)
        if result:
            letter_mapping = dict(zip(count(0), ascii_lowercase))
            original_code = result.group(1)
            print("Found code : {}".format(original_code))
            code_tab = [char for char in original_code]
            u0 = int(code_tab[1])
            u1 = int(code_tab[3])
            u2 = int(code_tab[4])
            nb1 = (u0 + u1 - u2) % 10
            nb2 = ((2 * u0) + (3 * u1**2) - (35 * u2**5)) % 10
            nb3 = (u0**2 + u1**3 + u2**4) % 26
            obtained_code = "{}{}{}{}{}{}".format(nb1, u0, nb2, u1, u2, letter_mapping[nb3].upper())
            print("Code verification by cacul : {}".format(obtained_code))
            if original_code == obtained_code:
                return True
        return False


'''
def split_str_by_comma(string, max_length):
    words = string.split(separator, maxsplit)
'''

if __name__ == '__main__':

    import json
    codes = openfoodfacts.facets.get_packaging_codes()
    product = openfoodfacts.products.get_product("80247937043707854551")
    with open('file.txt', 'w') as file:
        for code in codes:
            file.write(json.dumps(code['id']))
            file.write('\n')


    print(generate_emb_codes())
    print(check_emb_codes(generate_emb_codes()))

    for code in codes:
        f.write(code['id'])
        if check_emb_codes(code['id']):
            print(code['id'])

 
    store_keys(generate_key_pair(4096))
    keys_tuple = read_keys()
    public_key = keys_tuple[0]
    private_key = keys_tuple[1]

    message = 'encrypt me!'
    cipher = os.path.join(os.path.dirname(os.path.realpath('__file__')), 'ciphers/ingredients')

    encrypt = rsa_encrypt(message, public_key, b64_encode=True)
    obfuscate = text_obfuscate(encrypt, cipher)
    joined_string = ",".join(obfuscate)

    desobfuscate = text_desobfuscate(obfuscate, cipher)
    decrypt = rsa_decrypt(desobfuscate, private_key, b64_decode=True)

    print(encrypt)
    print("------------------")
    print(obfuscate)
    print("------------------")
    print(joined_string)
    print("------------------")
    print(desobfuscate)
    print("------------------")
    print(decrypt)
    # product_barcode = random_with_n_digits(20)
    product_barcode = 80247937043707854551
    # product_name = generate_word(5)
    product_name = "qauwi"
    emb_codes = generate_emb_codes()
    print("{}, {}".format(product_barcode, product_name))

    status_code = openfoodfacts.products.add_new_product({'code': product_barcode,
                                                          'product_name': product_name,
                                                          'ingredients_text': joined_string,
                                                          'emb_codes': emb_codes}
                                                        )
