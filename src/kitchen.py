# -*- coding: utf-8 -*-
import openfoodfacts


def allUnique(x):
    seen = list()
    return not any(i in seen or seen.append(i) for i in x)


'''

with open("ingredients") as file:
    chk_file = file.readlines()

print(chk_file)
print(allUnique(chk_file))


import collections
print([item for item, count in collections.Counter(chk_file).items() if count > 1])

product = openfoodfacts.products.advanced_search({
  "search_terms":"",
  "nutriment_0":"vitamin-b2",
  "nutriment_compare_0":"gt",
  "nutriment_value_0":"400",
  "sort_by":"unique_scans",
  "page_size":"20"
})
'''
codes = openfoodfacts.facets.get_packaging_codes()
for code in codes:
    print(code['id'])
