# ExfilCommis

Data exfiltration with openfoodfact

## Data flow

![](data_flow.png)

## Data recovery

The data is uploaded to OpenFoodFact anonymously by the client. The customer generates a product name and random information to make the product look real and to add entropy.

In order for the server to find the products uploaded by the clients, a mathematical sequence has been determined to define the emb_codes field and correspond to the syntax of the [French packaging codes](https://fr.wikipedia.org/wiki/EMB_(code)).

### Formula to get the packaging code

This code is composed of $`5`$ digits between $`0`$ and $`9`$, a letter and is preceded by the characters EMB.

So we set $`3`$ random numbers between $`0`$ and $`9`$ called $`u_0`$, $`u_1`$ and $`u_2`$. It then remains only to determine $`2`$ other numbers from these and a letter.

```math

nb_1 = u_0 + u_1 - u_2 \pmod{10}  \\
nb_2 = 2u_0 + 3{u_1}^2 - 35{u_2}^5 \pmod{10} \\
letter = {u_0}^2 + {u_1}^3 + {u_2}^4\pmod{26}
```
The code is then obtained by concatenating $`nb_1`$, $`u_0`$, $`nb_2`$, $`u_1`$, $`u_2`$ and $`let`$

### Number of combinations

Number of possible combinations is therefore equal to $`A_{10}^3 = \frac{10!}{7!} = 720`$